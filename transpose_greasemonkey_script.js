// ==UserScript==
// @name     Transpose my songs
// @version  1
// @grant    Vitalii
// @require http://code.jquery.com/jquery-latest.js
// ==/UserScript==

var guitarsong_transpose_current_transposition = 0;

function transposeChord(chord, amount) {
  var scale = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"];
  return chord.replace(/[CDEFGAB]#?/g,
                       function(match) {
                         var i = (scale.indexOf(match) + amount) % scale.length;
                         return scale[ i < 0 ? i + scale.length : i  ];
                       });
};


$(document).ready(function() {
  const my_edit_form = "<div>"
    + "<input type='text' id='guitarchords_transpose_val' value='0'>"
    + "<button id='guitarchords_transpose_btn'>Transpose</button>"
    + "<span>Current: <span id='guitarchords_current_transposition'>0</span></span>"
    + "<span id='guitarchords_error' class='error'></span>"
    + "<div>";

  $('.guitarchord').css('background-color', '#DDD');
  $(".guitarsong").before(my_edit_form);
  
  $("#guitarchords_transpose_btn").click(function(){
    var tr_val = parseInt($("#guitarchords_transpose_val").val(), 10);
    $('#guitarchords_current_transposition').text(guitarsong_transpose_current_transposition + " (requested " +  $("#guitarchords_transpose_val").val() + ")");
    

    
    if (tr_val === parseInt(tr_val, 10)) {
      guitarsong_transpose_current_transposition += tr_val;
      $( '.guitarchord' ).each(function(index) {
        var old_chord = $(this).text();
        
        $(this).text(transposeChord(old_chord, tr_val));
      });
      $('#guitarchords_current_transposition').text(guitarsong_transpose_current_transposition);
      console.log("dumping" + guitarsong_transpose_current_transposition);
    } else {
      $('#guitarchords_error').text("please provide integer");
    }
    



      

    
  });
  
});