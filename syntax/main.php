<?php
/**
* Plugin Guitarsong: Insert lyrics with tabs.
*
* @license    AGPL 3 https://www.gnu.org/licenses/agpl.html
* @author     Vitalii Minnakhmetov <restlessmonkey@ya.ru>
*/



// must be run within DokuWiki
if(!defined('DOKU_INC')) die();



class syntax_plugin_guitarsong_main extends DokuWiki_Syntax_Plugin {

    public function getType() { return 'formatting'; }
    public function getSort() { return 25; }

    function getAllowedTypes() {
        return array('formatting');
    }

    public function connectTo($mode) {
        $this->Lexer->addEntryPattern('<guitarsong>(?=.*?<\/guitarsong>)',
                                      $mode,
                                      'plugin_guitarsong_main');
    }

    function postConnect() {
        $this->Lexer->addExitPattern('</guitarsong>','plugin_guitarsong_main');
    }

    public function handle($match, $state, $pos, Doku_Handler $handler) {
        switch ($state) {
        case DOKU_LEXER_ENTER:
            return array($state, trim($match));

        case DOKU_LEXER_UNMATCHED :
            return array($state, $match);

        case DOKU_LEXER_EXIT:
            return array($state,'');

        default:
            return array($state,$match);
        }
    }


    public function render($mode, Doku_Renderer $renderer, $data) {
        if($mode == 'xhtml'){

            list($state, $match) = $data;

            $guitarsong = plugin_load('helper', 'guitarsong');

            switch ($state) {
                case DOKU_LEXER_ENTER :
                    $renderer->doc .= '<code class="guitarsong">';
                    break;
                case DOKU_LEXER_UNMATCHED :
                    $match = $renderer->_xmlEntities($match);

                    // regexp taken from https://stackoverflow.com/a/29145150/4294001
                    $match = preg_replace('/\b([CDEFGAB](?:b|bb)*(?:#|##|sus|maj|min|aug|m)*[\d\/]*(?:[CDEFGAB](?:b|bb)*(?:#|##|sus|maj|min|aug)*[\d\/]*)*)(?=\s|$)/',
                                          $guitarsong::chord_html_full,
                                          $match);
                    $renderer->doc .= $match;
                    break;
                case DOKU_LEXER_EXIT :
                    $renderer->doc .= "</code>";
                    break;
            }
            return true;
        }
        return false;
    }
}
