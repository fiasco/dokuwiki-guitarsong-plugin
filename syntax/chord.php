<?php
/**
* Plugin Guitarsong: Insert lyrics with tabs.
*
* @license    AGPL 3 https://www.gnu.org/licenses/agpl.html
* @author     Vitalii Minnakhmetov <restlessmonkey@ya.ru>
*/



// must be run within DokuWiki
if(!defined('DOKU_INC')) die();

require_once(DOKU_PLUGIN.'syntax.php');

class syntax_plugin_guitarsong_chord extends DokuWiki_Syntax_Plugin {

    public function getType() { return 'formatting'; }
    public function getSort() { return 24; }

    function getAllowedTypes() {
        return array('formatting');
    }


    public function connectTo($mode) {
        $this->Lexer->addEntryPattern('<chord>(?=.*?<\/chord>)',
                                      $mode,
                                      'plugin_guitarsong_chord');
    }

    function postConnect() {
        $this->Lexer->addExitPattern('</chord>','plugin_guitarsong_chord');
    }

    // TODO: use some common idiom for this method
    public function handle($match, $state, $pos, Doku_Handler $handler) {
        switch ($state) {
        case DOKU_LEXER_ENTER:
            return array($state, trim($match));

        case DOKU_LEXER_UNMATCHED :
            return array($state, $match);

        case DOKU_LEXER_EXIT:
            return array($state,'');

        default:
            return array($state,$match);
        }
    }

    public function render($mode, Doku_Renderer $renderer, $data) {
        if($mode == 'xhtml'){

            list($state, $match) = $data;

            $guitarsong = plugin_load('helper', 'guitarsong');
            switch ($state) {
                case DOKU_LEXER_ENTER :
                    $renderer->doc .= $guitarsong::chord_html_start_string;
                    break;
                case DOKU_LEXER_UNMATCHED :
                    $match = $renderer->_xmlEntities($match);
                    $renderer->doc .= $match;
                    break;
                case DOKU_LEXER_EXIT :
                    $renderer->doc .= $guitarsong::chord_html_end_string;
                    break;
            }
            return true;
        }
        return false;
    }
}
