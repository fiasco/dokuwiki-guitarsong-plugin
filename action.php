<?php
/**
 * Plugin Guitarsong: Insert lyrics with tabs.
 *
 * @license    AGPL 3 https://www.gnu.org/licenses/agpl.html
 * @author     Vitalii Minnakhmetov <restlessmonkey@ya.ru>
 */

 
// Make sure we run within DokuWiki
if (!defined('DOKU_INC')) die();

class action_plugin_guitarsong extends DokuWiki_Action_Plugin {

    public function register(Doku_Event_Handler $controller) {
        $controller->register_hook('TOOLBAR_DEFINE', 'AFTER', $this, 'insert_button');
    }


    public function insert_button(Doku_Event $event, $param) {
        $event->data[] = array (
            'type' => 'format',
            'title' => $this->getLang('guitarsong_toolbar_button'),
            'icon' => '../../plugins/guitarsong/images/guitarsong_toolbar_button_16.png',
            'open' => '<guitarsong>',
            'close' => '</guitarsong>',
        );
    }

}