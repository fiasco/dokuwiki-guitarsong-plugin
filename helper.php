<?php
/**
* Plugin Guitarsong: Insert lyrics with tabs.
*
* @license    AGPL 3 https://www.gnu.org/licenses/agpl.html
* @author     Vitalii Minnakhmetov <restlessmonkey@ya.ru>
*/


// must be run within Dokuwiki
if(!defined('DOKU_INC')) die();

class helper_plugin_guitarsong extends DokuWiki_Plugin {
    const chord_html_start_string = '<span class="guitarchord">';
    const chord_html_end_string = '</span>';
    
    const chord_html_full = self::chord_html_start_string . '$0' . self::chord_html_end_string;
    
}